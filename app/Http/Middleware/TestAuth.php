<?php

namespace App\Http\Middleware;

use Closure;

class TestAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( 20 > 10 ) {
            dd(false);
        }
        else {
            return $next($request);
        }
    }
}
